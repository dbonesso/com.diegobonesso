const express = require('express');
const bodyParser = require('body-parser')
const app = express();

var mongo = require('mongodb');
var monk = require('monk');
var db = monk('mongodb://experimental:data103@ds011024.mlab.com:11024/experimental');

var session = require('express-session')


app.use(bodyParser.urlencoded({extended: true}))
app.use(express.static(__dirname + '/public'));
app.set('view engine','pug');



function restrict(req, res, next) {
  if (req.session.user) {
    next();
  } else {
    req.session.error = 'Access denied!';
    res.redirect('/login');
  }
}




app.listen(3000, ()=>{
            console.log('Listening on 3000');
          });


app.use(session({
  secret: 'Bq9bGqETYtynbSsS3tYc',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true },

}))



app.get('/',(req,res) =>{
    //res.sendFile('index.html');
    res.render('login.pug',{ title : 'Experiment.' })
})



app.get('/instructions',restrict,(req,res) =>{

    res.render('instructions.pug',{ title : 'Instructions.' })
})


app.get('/researchform',restrict,(req,res) =>{

    res.render('researchform.pug',{ title : 'Reviews.' })
})


app.get('/login',(req,res) =>{
    res.render('login');
})



//create the user and send to judgment.
app.post('/login',(req,res) =>{

   db.get('user').findOne({user : req.body['name']},function(error, auth){
      //if user already exist!!!
      if (auth != null) {
        //get the user name and continue validation.
        // take the first review which are not analized yet.
        db.get(req.body['name']).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
               req.session.user = auth;
               console.log(firstReview)
               res.render('researchform.pug',firstReview);
        });

      } else {
                  var user ={
                        judge : req.body['name']
                  };
                  res.render('createUser.pug',user);
     }
   });
});






//this function is called after read the instructions.
app.post('/instructions',(req,res) =>{

     db.get(req.body['judge']).findOne({user : req.body['name']},function(error, doc){
        //if user already exist!!!
        if (doc != null) {
          //get the user name and continue validation.
          // take the first review which are not analized yet.
          db.get(req.body['judge']).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
                 res.render('researchform.pug',firstReview);
          });
        }
    });



});





//save the  judgment and goto the next.
app.post('/savejudgement',(req,res) =>{

     var review = {
       id    : req.body['id'],
       judge : req.body['judge'],
       value : req.body['value']===undefined ? false : true ,
       location : req.body['location']===undefined ? false : true,
       room : req.body['room']===undefined ? false : true,
       clean : req.body['clean']===undefined ? false : true,
       service : req.body['service']===undefined ? false : true,
       sleep : req.body['sleep']===undefined ? false : true,
       facility : req.body['facility']===undefined ? false : true,
       weightvalue : req.body['weightvalue'],
       weightlocation : req.body['weightlocation'],
       weightroom : req.body['weightroom'],
       weightclean : req.body['weightclean'],
       weightservice : req.body['weightservice'],
       weightfacility : req.body['weightfacility'],
       weightfood : req.body['weightfood']
     };
     console.log(review);
     db.get('judgement').insert(review ,function (err, result){
       db.get(req.body['judge']).update(
                    {id : req.body['id']},
                    {$set:{analized:"true" }},
                    function (err, result) {
                        if (err) throw err;
                        db.get(req.body['judge']).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
                             if (err) throw err;
                             console.log(firstReview);
                             res.render('researchform.pug',firstReview);
                        });


                    });

     });
});


//save the judgment and close section.
app.post('/closeSessionSave',(req,res) =>{
  var review = {
    id    : req.body['id'],
    judge : req.body['judge'],
    value : req.body['value']===undefined ? false : true ,
    location : req.body['location']===undefined ? false : true,
    room : req.body['room']===undefined ? false : true,
    clean : req.body['clean']===undefined ? false : true,
    service : req.body['service']===undefined ? false : true,
    sleep : req.body['sleep']===undefined ? false : true,
    facility : req.body['facility']===undefined ? false : true,
    weightvalue : req.body['weightvalue'],
    weightlocation : req.body['weightlocation'],
    weightroom : req.body['weightroom'],
    weightclean : req.body['weightclean'],
    weightservice : req.body['weightservice'],
    weightfacility : req.body['weightfacility'],
    weightfood : req.body['weightfood']
  };
  console.log(review);
  db.get('judgement').insert(review ,function (err, result){
    db.get(req.body['judge']).update(
                 {id : req.body['id']},
                 {$set:{analized:"true" }},
                 function (err, result) {
                     if (err) throw err;
                     req.session.destroy(function(err) {
                         if(err) {
                           console.log(err);
                         } else {
                           res.redirect('/');
                         }
                     })


                 });

  });





});



//save the judgment and close section.
app.post('/createUser',(req,res) =>{
  console.log(req.body['judge']);
  //take the collection which are not analyzed yet or have the minor number of analyzes
  db.get('reviewExperimentControl').findOne({"$query":{},"$orderby":{"total":1}},function(error, resultGroup){
      //take all reviews of the previus select group

      db.get('review').find({ group: resultGroup["group"] },function(error, resultReview) {
             if(error) throw err;
             var countJudge = resultGroup['total'] + 1;
             var activateStarts = 'false';
             //if the analyses group count is odd then activate the stars.
             if (Math.abs(countJudge % 2) == 1){
                 activateStarts = 'true';
             }


             db.get('reviewExperimentControl').update(
                          { group : resultGroup["group"]},
                          { $set: { total: countJudge } },
                          function (err, result) {
                              if (err) throw err;
                              var user ={
                                  user : req.body['judge'],
                                  group : resultGroup["group"],
                                  completed : "false"
                              };
                              db.get('user').insert(user ,function (err, result) {
                                  if (err) throw err;
                                  db.get(req.body['judge']).insert(resultReview ,function (err, result) {
                                      if (err) throw err;
                                      console.log(result);
                                      db.get(req.body['judge']).update(
                                                   {},
                                                   {$set:{analized:"false", judge: req.body['judge'],stars: activateStarts  }},
                                                   { multi: true },
                                                   function (err, result) {
                                                       if (err) throw err;
                                                       db.get(req.body['judge']).findOne({ analized: { $eq: "false" } },function(err, firstReview) {
                                                            if (err) throw err;
                                                             var data ={
                                                                  id : firstReview['id'],
                                                                  judge : req.body['judge']

                                                             };

                                                             console.log(firstReview);
                                                             res.render('instructions',data);
                                                       });


                                                   });
                                  });
                              });
              })







      });

  });


});
